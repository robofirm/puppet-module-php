# Configures a PHP FPM process pool
define php::fpm_pool (
  Enum['absent', 'present'] $ensure = 'present',
  Hash $settings = {},
) {

  $fpm_conf_notify = $ensure ? {
    'absent' => [],
    default => Service['php-fpm'],
  }
  file { "/etc/php/php-fpm.d/${name}.conf":
    ensure  => $ensure,
    require => Package['php-fpm'],
    notify  => $fpm_conf_notify,
  }

  if ($ensure == 'present' and !empty($settings)) {

    if ($settings['group'] != undef or
      $settings['php_value[error_log]'] != undef or
      $settings['php_admin_value[error_log]'] != undef or
      $settings['php_value[session.save_path]'] != undef or
      $settings['php_admin_value[session.save_path]'] != undef or
      $settings['php_value[soap.wsdl_cache_dir]'] != undef or
      $settings['php_admin_value[soap.wsdl_cache_dir]'] != undef
    ) {
      if ($settings['group'] == undef) {
        # If using the default group, but defining custom file locations, we must fail because we cannot ensure file
        # writability, since we don't know the default group
        fail("\"group\" must be set in PHP FPM pool \"${name}\" settings when php_value[error_log],
          php_admin_value[error_log], php_value[session.save_path], php_admin_value[session.save_path],
          php_value[soap.wsdl_cache_dir], or php_admin_value[soap.wsdl_cache_dir] is set. This module does
          not assume a group name, but must ensure these files are writable.")
      }

      # If using a custom group, we need to make sure the log file, session save path, and wsdl cache dir are writable.
      # Since we cannot know them, we must make some defaults so that all values can be known.
      $admin_error_log_setting = 'php_admin_value[error_log]'
      $error_log_setting = 'php_value[error_log]'
      if ($settings[$admin_error_log_setting] != undef) {
        $error_log_is_admin = true
        $error_log = $settings[$admin_error_log_setting]
      } elsif ($settings[$error_log_setting] != undef) {
        $error_log_is_admin = false
        $error_log = $settings[$error_log_setting]
      } else {
        $error_log_is_admin = false
        $error_log = "/var/log/php-fpm/${name}-error.log"
      }

      $admin_session_save_path_setting = 'php_admin_value[session.save_path]'
      $session_save_path_setting = 'php_value[session.save_path]'
      if ($settings[$admin_session_save_path_setting] != undef) {
        $session_save_path_is_admin = true
        $session_save_path = $settings[$admin_session_save_path_setting]
      } elsif ($settings[$session_save_path_setting] != undef) {
        $session_save_path_is_admin = false
        $session_save_path = $settings[$session_save_path_setting]
      } else {
        $session_save_path_is_admin = false
        $session_save_path = "/var/lib/php/${name}-session"
      }

      $admin_soap_wsdl_cache_dir_setting = 'php_admin_value[soap.wsdl_cache_dir]'
      $soap_wsdl_cache_dir_setting = 'php_value[soap.wsdl_cache_dir]'
      if ($settings[$admin_soap_wsdl_cache_dir_setting] != undef) {
        $soap_wsdl_cache_dir_is_admin = true
        $soap_wsdl_cache_dir = $settings[$admin_soap_wsdl_cache_dir_setting]
      } elsif ($settings[$soap_wsdl_cache_dir_setting] != undef) {
        $soap_wsdl_cache_dir_is_admin = false
        $soap_wsdl_cache_dir = $settings[$soap_wsdl_cache_dir_setting]
      } else {
        $soap_wsdl_cache_dir_is_admin = false
        $soap_wsdl_cache_dir = "/var/lib/php/${name}-wsdlcache"
      }

      $resolved_settings = merge(
        $error_log_is_admin ? {
          true    => { 'php_admin_value[error_log]' => $error_log },
          default => { 'php_value[error_log]' => $error_log },
        },
        $session_save_path_is_admin ? {
          true    => { 'php_admin_value[session.save_path]' => $session_save_path },
          default => { 'php_value[session.save_path]' => $session_save_path },
        },
        $soap_wsdl_cache_dir_is_admin ? {
          true    => { 'php_admin_value[soap.wsdl_cache_dir]' => $soap_wsdl_cache_dir },
          default => { 'php_value[soap.wsdl_cache_dir]' => $soap_wsdl_cache_dir },
        },
        $settings
      )

      file { [
        $error_log,
      ]:
        ensure  => file,
        group   => $settings['group'],
        require => Package['php-fpm'],
        mode    => '0660',
      }

      file { [
        $session_save_path,
        $soap_wsdl_cache_dir,
      ]:
        ensure  => directory,
        group   => $settings['group'],
        require => Package['php-fpm'],
        mode    => '0770',
      }
    } else {
      $resolved_settings = $settings
    }

    create_ini_settings({ $name => $resolved_settings}, {
      path => "/etc/php/php-fpm.d/${name}.conf",
      require => File["/etc/php/php-fpm.d/${name}.conf"],
      notify => Service['php-fpm'],
    })
  }
}
