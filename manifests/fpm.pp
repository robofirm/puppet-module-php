# Install and configure PHP FPM
class php::fpm (
  Enum['absent', 'present', 'latest'] $ensure = 'present',
  Float $version                              = 7.2,
  Hash $service                               = {
    enable => true,
    ensure => running,
  },
  Hash $settings                              = { },
  Hash $cli_settings                          = { },
) {

  case ($::facts['osfamily']) {
    'RedHat': {
      case ($version) {
        # @todo Deal with 64 vs. 32 bit for $lib_dir?
        5.5: {
          $package_prefix = 'php55'
          $etc_dir = "/opt/remi/${package_prefix}/root/etc/"
          $lib_dir = "/opt/remi/${package_prefix}/root/lib64/php/"
          $log_dir = "/opt/remi/${package_prefix}/root/var/log/php-fpm"
        }
        5.6: {
          $package_prefix = 'php56'
          $etc_dir = "/opt/remi/${package_prefix}/root/etc/"
          $lib_dir = "/opt/remi/${package_prefix}/root/lib64/php/"
          $log_dir = "/opt/remi/${package_prefix}/root/var/log/php-fpm"
        }
        7.0: {
          $package_prefix = 'php70'
          $etc_dir = "/etc/opt/remi/${package_prefix}/"
          $lib_dir = "/var/opt/remi/${package_prefix}/lib/php/"
          $log_dir = "/var/opt/remi/${package_prefix}/log/php-fpm"
        }
        7.1: {
          $package_prefix = 'php71'
          $etc_dir = "/etc/opt/remi/${package_prefix}/"
          $lib_dir = "/var/opt/remi/${package_prefix}/lib/php/"
          $log_dir = "/var/opt/remi/${package_prefix}/log/php-fpm"
        }
        7.2: {
          $package_prefix = 'php72'
          $etc_dir = "/etc/opt/remi/${package_prefix}/"
          $lib_dir = "/var/opt/remi/${package_prefix}/lib/php/"
          $log_dir = "/var/opt/remi/${package_prefix}/log/php-fpm"
        }
        default: {
          fail("Unsupported PHP version ${version} for RedHat.")
        }
      }

      package { "${package_prefix}-php-cli":
        ensure => $ensure,
        alias  => 'php-cli',
      }

      $bin_php_link_ensure = $ensure ? {
        absent  => 'absent',
        default => 'link'
      }
      file { '/usr/bin/php':
        ensure  => $bin_php_link_ensure,
        target  => "/usr/bin/${package_prefix}",
        require => Package['php-cli'],
      }

      package { "${package_prefix}-php-fpm":
        ensure => $ensure,
        alias  => 'php-fpm',
      }

      if ('absent' != $ensure) {
        service { 'php-fpm':
          * => merge({
            require => Package['php-fpm'],
            status  => 'service php-fpm status | grep "running"'
          }, $service)
        }
      }

      $fpm_service_link_ensure = $ensure ? {
        absent  => 'absent',
        default => 'link'
      }
      file { '/usr/lib/systemd/system/php-fpm.service':
        ensure  => $fpm_service_link_ensure,
        target  => "/usr/lib/systemd/system/${package_prefix}-php-fpm.service",
        require => Package['php-fpm'],
      }

      if ($etc_dir != '/etc/php') {
        $etc_dir_link_ensure = $ensure ? {
          absent  => 'absent',
          default => 'link'
        }
        file { '/etc/php':
          ensure  => $etc_dir_link_ensure,
          target  => $etc_dir,
          force   => true,
          require => [
            Package['php-cli'],
            Package['php-fpm'],
          ],
        }
      }

      if ($lib_dir != '/var/lib/php') {
        $lib_dir_link_ensure = $ensure ? {
          absent  => 'absent',
          default => 'link'
        }
        file { '/var/lib/php':
          ensure  => $lib_dir_link_ensure,
          target  => $lib_dir,
          force   => true,
          require => [
            Package['php-cli'],
            Package['php-fpm'],
          ],
        }
      }

      if ($log_dir != '/var/log/php-fpm') {
        $fpm_log_link_ensure = $ensure ? {
          absent  => 'absent',
          default => 'link'
        }
        file { '/var/log/php-fpm':
          ensure  => $fpm_log_link_ensure,
          target  => $log_dir,
          force   => true,
          require => [
            Package['php-cli'],
            Package['php-fpm'],
          ],
        }
      }
    }
    default: {
      fail("Unsupported osfamily: ${::facts['osfamily']}.")
    }
  }

  if ($ensure != 'absent') {
    if (!empty($settings)) {
      create_ini_settings($settings, {
        path    => '/etc/php/php.ini',
        require => Package['php-fpm'],
        notify  => Service['php-fpm'],
      })
    }

    if (!empty($cli_settings)) {
      create_ini_settings($cli_settings, {
        path    => '/etc/php/php-cli.ini',
        require => Package['php-cli'],
      })
    }
  }
}
