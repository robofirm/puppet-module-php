# Installs and configures a PHP extension
define php::extension (
  Enum['absent', 'present', 'latest'] $ensure = 'present',
  Float $php_version                          = 7.2,
  Hash $settings                              = { },
  Hash $cli_settings                          = { },
) {
  case ($::facts['osfamily']) {
    'RedHat': {
      case ($php_version) {
        5.5: {
          $package_prefix = 'php55'
        }
        5.6: {
          $package_prefix = 'php56'
        }
        7.0: {
          $package_prefix = 'php70'
        }
        7.1: {
          $package_prefix = 'php71'
        }
        7.2: {
          $package_prefix = 'php72'
        }
        default: {
          fail("Unsupported PHP version ${php_version} for RedHat.")
        }
      }

      $php_extension_notify = $ensure ? {
        'absent' => [],
        default  => Service['php-fpm'],
      }
      package { "${package_prefix}-php-${name}":
        ensure            => $ensure,
        alias             => "php-${name}",
        require           => Package['php-fpm'],
        notify            => $php_extension_notify,
      }
    }
    default: {
      fail("Unsupported osfamily: ${::facts['osfamily']}.")
    }
  }

  if ('absent' != $ensure) {
    # @todo Determine how to deal with priority (e.g. 20-)
    if (!empty($settings)) {
      create_ini_settings($settings, {
        path    => "/etc/php/php.d/20-${name}.ini",
        require => Package['php-fpm'],
        notify  => Service['php-fpm'],
      })
    }

    if (!empty($cli_settings)) {
      create_ini_settings($cli_settings, {
        path    => "/etc/php/php-cli.d/20-${name}.ini",
        require => Package['php-cli'],
      })
    }
  }
}
