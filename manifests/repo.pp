# Configures required repos to install PHP FPM packages
class php::repo (
  Float $version = 7.2,
) {

  case ($::facts['osfamily']) {
    'RedHat': {
      contain yum::repo::epel
      contain yum::repo::remi
      case ($version) {
        5.5: {
          contain yum::repo::remi_php55
        }
        5.6: {
          contain yum::repo::remi_php56
        }
        7.0: {
          contain yum::repo::remi_php70
        }
        7.1: {
          contain yum::repo::remi_php71
        }
        7.2: {
          # example42/yum is no longer supported and, thus, has no php 7.2 predefined repo
          $releasever = $::operatingsystem ? {
            /(?i:Amazon)/ => '6',
            default       => '$releasever',  # Yum var
          }

          yum::managed_yumrepo { 'remi-php72':
            descr      => 'Remi\'s PHP 7.2 RPM repository for Enterprise Linux $releasever - $basearch',
            mirrorlist => "http://rpms.remirepo.net/enterprise/${releasever}/php71/mirror",
            enabled    => 1,
            gpgcheck   => 1,
            gpgkey     => 'http://rpms.remirepo.net/RPM-GPG-KEY-remi',
            priority   => 1,
          }
        }
        default: {
          fail("Unsupported PHP version ${version} for RedHat.")
        }
      }
    }
    default: {
      fail("Unsupported osfamily: ${::facts['osfamily']}.")
    }
  }
}
